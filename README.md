# Kubernetes

After every pi was installed, I add my ssh-key to the `authorized_keys` on each one:

```
ssh-copy-id ubuntu@pi0x0[0-4].k8s
```

## Define hosts file

Example:

```
[control_plane]
pi0x00.k8s ansible_user=ubuntu

[workers]
pi0x01.k8s ansible_user=ubuntu
pi0x02.k8s ansible_user=ubuntu
pi0x03.k8s ansible_user=ubuntu
pi0x04.k8s ansible_user=ubuntu
```


## Running ansible playbooks

- installation.yaml
- control-plane.yaml
- workers.yaml

## Check nodes

```
$ kubectl get nodes
NAME     STATUS   ROLES           AGE   VERSION
pi0x00   Ready    control-plane   50m   v1.24.1
pi0x01   Ready    <none>          31m   v1.24.1
pi0x02   Ready    <none>          31m   v1.24.1
pi0x03   Ready    <none>          31m   v1.24.1
pi0x04   Ready    <none>          31m   v1.24.1
```
